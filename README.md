## Snake array 2 task

### Create a function that returns a spiral matrix (two-dimensional array) with 6 columns and 5 rows. See image below for the pattern.

![snake-array-example](./snake-array-example.PNG)
